import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Utils {

	private static final int TAB_SIZE = 4;

	public static void sleep(int seconds) {
		try {
			TimeUnit.SECONDS.sleep(seconds);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public static String process(int i) {
		Utils.printPerform(i);

		Utils.sleep(i);

		return i + " successful";
	}

	public static List<Integer> getDataForProcessing() {
		return IntStream.iterate(1, i -> i + 2).limit(10).boxed().collect(Collectors.toList());
	}

	public static void printSubmit(int i) {
		printAction(i, "submit");
	}

	public static void printPerform(int i) {
		printAction(i, "perform");
	}

	public static void printApplyTimeout(int i) {
		printAction(i, "timeout");
	}

	public static void printComplete(String result) {
		String[] split = result.split(" ");

		printAction(Integer.parseInt(split[0]), "completed");
	}

	public static void printResults(List<String> list) {
		System.out.println("Results: ");
		list.forEach(System.out::println);
	}

	private static void printAction(int i, String action) {
		System.out.println(Utils.getTabulation(i) + " " + action);
	}

	private static String getTabulation(int i) {
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < i; j++) {
			sb.append(" ".repeat(TAB_SIZE));
		}
		sb.append(i);
		return sb.toString();
	}
}
