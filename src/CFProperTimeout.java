import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class CFProperTimeout {
	private static final int TIMEOUT = 10;

	private static final ExecutorService EXECUTOR = Executors.newCachedThreadPool();
	private static final ExecutorService AUX_EXECUTOR = Executors.newFixedThreadPool(2);

	public static void main(String[] args) {
		List<Integer> data = Utils.getDataForProcessing();

		List<String> results = data
			.parallelStream()
			.map(i -> CompletableFuture.supplyAsync(
				() -> asyncProcessInteger(i),
				AUX_EXECUTOR)
			)
			.map(CompletableFuture::join)
			.sorted()
			.collect(Collectors.toList());

		Utils.printResults(results);

		EXECUTOR.shutdown();
		AUX_EXECUTOR.shutdown();
	}

	private static String asyncProcessInteger(int i) {
		Utils.printSubmit(i);

		CompletableFuture<String> cf = CompletableFuture.supplyAsync(
			() -> Utils.process(i),
			EXECUTOR
		);

		Utils.printApplyTimeout(i);

		String result = cf.completeOnTimeout(i + " timedout", TIMEOUT, TimeUnit.SECONDS).join();

		Utils.printComplete(result);

		return result;
	}
}
