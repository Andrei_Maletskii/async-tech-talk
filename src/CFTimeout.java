import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class CFTimeout {
	private static final int TIMEOUT = 10;

	private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(2);

	public static void main(String[] args) {

		List<Integer> data = Utils.getDataForProcessing();

		List<String> results = data.parallelStream()
			.map(CFTimeout::asyncProcessInteger)
			.map(cf -> {
				String result = cf.join();

				Utils.printComplete(result);

				return result;
			})
			.sorted()
			.collect(Collectors.toList());

		Utils.printResults(results);

		EXECUTOR.shutdown();
	}

	private static CompletableFuture<String> asyncProcessInteger(int i) {
		Utils.printSubmit(i);

		CompletableFuture<String> cf = CompletableFuture.supplyAsync(
			() -> Utils.process(i),
			EXECUTOR
		);

		Utils.printApplyTimeout(i);

		return cf.completeOnTimeout(i + " timedout", TIMEOUT, TimeUnit.SECONDS);
	}
}
